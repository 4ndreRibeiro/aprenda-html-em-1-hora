# Aprenda HTML em 1 hora

## Aula 01 - introdução ao curso.
## Aula 02 - Tags.
## Aula 03 - Criando nossa primeira página em HTML.
## Aula 04 - Títulos, negrito, itálico e comentários.
## Aula 05 - Atributos.
## Aula 06 - Criando links
## Aula 07 - Trabalho com imagens
## Aula 08 - Codificações, folhas de estilos e scripts.
## Aula 09 - divs.
## Aula 10 - Tabelas.
## Aula 11 - Atributos das tabelas.
## Aula 12 - Listas
## Aula 13 - Introdução a formularios.
## Aula 14 - Campos do formulários.
## Aula 15 - Labels e textareas.
## Aula 16 - Checkboxes e Radios Buttons.
## Aula 17 - Dropdown list e passwords.
## Aula 18 - File inputs.
## Aula 19 - Botões.
## Aula 20 - Criando um formulário completo.
## Aula 21 - Como publicar seu site na internet.
## Aula 22 - Considerações finais.